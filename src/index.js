const express = require('express');
const path = require('path');
const morgan = require('morgan');
const multer = require('multer');
const {v4} = require('uuid');

//Initial Part
const app = express();
require('./database')
//Settings Part
app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');
app.set('port',process.env.PORT || 4000);
//Middlewares Part
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
const storage = multer.diskStorage({
    destination: path.join(__dirname,'public/img/uploads'),
    filename: (req,file,cb, filename) => {
        cb(null,v4() + path.extname(file.originalname))
    }
})
app.use(multer({storage}).single('image'));
//Global Variable Part
//Routes
app.use(require('./routes/index'));
//Static files Part
app.use(express.static(path.join(__dirname, 'public')));
//Start Server
app.listen(app.get('port'),() => {
    console.log(`Server on port: ${app.get('port')}`)
})
const { Router } = require('express');
const router = Router();
const Image = require('../models/Image');
const { unlink } = require('fs-extra');
const path = require('path');

router.get('/', async(req,res) => {
    const images = await Image.find();
    res.render('index',{images});
})
router.get('/image/:id',async(req,res) => {
    const { id } = req.params;
    const image = await Image.findById(id);
    res.render('product',{image})
})
router.get('/upload',(req,res) => {
    res.render('upload')
})
router.post('/upload', async(req,res) => {
    const image = new Image(); 
    image.product = req.body.product;
    image.description = req.body.description; 
    image.price = req.body.price;
    image.filename = req.file.filename;
    image.path = '/img/uploads/' + req.file.filename;
    image.originalname = req.file.originalname;
    image.mimetype = req.file.mimetype;
    image.size = req.file.size;
    await image.save();
    res.redirect('/')
})

router.post('/actualiza', async(req,res) => {
    const image = req.body;
    console.log(image)
    await Image.findByIdAndUpdate(image.id,{
        product: image.product,
        description: image.description,
        price: image.price
    })
    res.redirect('/') 
})

router.get('/image/:id/delete',async(req,res) => {
    const { id } = req.params;
    const image = await Image.findByIdAndDelete(id);
    await unlink(path.resolve('./src/public'+ image.path));
    res.redirect('/');
})
router.get('/image/:id/edit',async(req,res) => {
    const { id } = req.params;
    const image = await Image.findById(id);
    res.render('editar',{image})
})
module.exports = router;